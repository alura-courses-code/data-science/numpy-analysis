# numpy-analysis

Basic numpy functions to day-day.

## Contributing

If you wish to contribute to this project, feel free to open a merge request. We welcome all forms of contribution!

## License

This project is licensed under the [MIT](https://gitlab.com/alura-courses-code/data-science/numpy-analysis/-/blob/main/LICENSE). Refer to the LICENSE file for more details.
